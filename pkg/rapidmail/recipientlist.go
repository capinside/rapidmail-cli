package rapidmail

// RecipientList ...
// https://developer.rapidmail.wiki/documentation.html?urls.primaryName=Recipientlists
type RecipientList struct {
	Created                 string `json:"created"`
	Default                 string `json:"default"`
	Description             string `json:"description"`
	ID                      uint   `json:"id"`
	Name                    string `json:"name"`
	RecipientSubscribeEmail string `json:"recipient_subscribe_email"`
	SubscribeFromFieldKey   string `json:"subscribe_form_field_key"`
	SubscribeFromURL        string `json:"subscribe_form_url"`
	UnsubscribedBlacklist   string `json:"unsubscribe_blacklist"`
	Updated                 string `json:"updated"`
}

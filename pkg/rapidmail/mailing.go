package rapidmail

import (
	"encoding/json"
	"fmt"
)

const (
	MailingsPath = "/mailings"
)

var (
	// Mailings service global variable
	Mailings MailingsService
)

// MailingsService interface
type MailingsService interface {
	// Search returns a pointer of MailingsResponse which contains mailings
	// matching params and nil or nil and an error
	Search(params MailingsRequestParams) (*MailingsResponse, error)
	// SearchAll returns a slice of all mailings matching params and nil
	// or nil and an error. Handles pagination.
	SearchAll(params MailingsRequestParams) ([]Mailing, error)
	// FetchByID returns a pointer of Mailing matching id and nil
	// or nil and an error
	FetchByID(id int) (*Mailing, error)
	// Recipients returns a pointer of MailingRecipientsResponse of mailing
	// matching id and nil or nil and an error
	Recipients(id int) (*MailingRecipientsResponse, error)
}

// MailingsServiceSearchFunc function header
type MailingsServiceSearchFunc func(MailingsRequestParams) (*MailingsResponse, error)

// MailingsServiceFetchByIDFunc function header
type MailingsServiceFetchByIDFunc func(int) (*Mailing, error)

// MailingsServiceRecipientsFunc function header
type MailingsServiceRecipientsFunc func(int) (*MailingRecipientsResponse, error)

// MailingsRequestParams used to filter mailings
type MailingsRequestParams struct {
	APIRequestParams
	// Filter collection by status
	Status string `json:"status" url:"status,omitempty"`
	// Filter collection by created datetime showing all mailings that were created since given datetime. Format: Y-m-d H:i:s
	CreatedSince string `json:"created_since" url:"created_since,omitempty"`
	// Filter collection by updated datetime showing all mailings that were updated since given datetime. Format: Y-m-d H:i:s
	UpdatedSince string `json:"updated_since" url:"updated_since,omitempty"`
	// Field to sort by, can be created, updated, sent or send_at
	SortBy string `json:"sort_by" url:"sort_by,omitempty"`
	// Order to sort in, can be asc or desc
	SortOrder string `json:"sort_order" url:"sort_order,omitempty"`
}

// MailingsResponse data
type MailingsResponse struct {
	APIResponse
	Embedded map[string][]Mailing `json:"_embedded"`
}

// Mailings returns a slice of Mailing embedded in response
func (mr *MailingsResponse) Mailings() []Mailing {
	return mr.Embedded["mailings"]
}

// NewMailingsService returns an instance of mailingsService that
// implements the MailingsService interface with default functions
func NewMailingsService(client Client) MailingsService {
	return NewMailingsServiceWithFunctions(
		mailingsServiceSearchFunc(client),
		mailingsServiceFetchByID(client),
		mailingsServiceRecipientsFunc(client),
	)
}

// NewMailingsServiceWithFunctions returns an instance of mailingsService that
// implements the MailingsService interface with custom functions
func NewMailingsServiceWithFunctions(searchFunc MailingsServiceSearchFunc, fetchByIDFunc MailingsServiceFetchByIDFunc, recipientsFunc MailingsServiceRecipientsFunc) MailingsService {
	return &mailingsService{
		fetchByIDFunc:  fetchByIDFunc,
		recipientsFunc: recipientsFunc,
		searchFunc:     searchFunc,
	}
}

type mailingsService struct {
	fetchByIDFunc  MailingsServiceFetchByIDFunc
	recipientsFunc MailingsServiceRecipientsFunc
	searchFunc     MailingsServiceSearchFunc
}

// Mailing ...
// https://developer.rapidmail.wiki/documentation.html?urls.primaryName=Mailings#/Mailings/get_mailings
type Mailing struct {
	// Number of attachments used
	Attachments int `json:"attachments,string"`
	// Datetime the mailing was canceled at. NULL if mailing is not canceled.
	Canceled bool `json:"canceled"`
	// Enable austrian ECG list checking (only for austria)
	CheckECG string `json:"check_ecg"`
	// Enable robinsonlist checking (only if applicable based on country)
	CheckRobinson string `json:"check_robinson"`
	// Created datetime
	Created string `json:"created"`
	// Datetime the mailing was deleted at. NULL if mailing is not deleted.
	Deleted string `json:"deleted"`
	// Specifies if the destination should be used as include or as exclude list. Please note that at least one include list needs to be specified. This feature may not be enabled for your account.
	Destination []MailingDestination `json:"destinations"`
	// Shows if the mailing is an A/B-Split mailing
	FeatureMailingSplit string `json:"feature_mailingsplit"`
	// Email address of mailing sender. Note that the address needs to be a confirmed sender address.
	FromEmail string `json:"from_email"`
	// 	// Name of mailing sender
	FromName string `json:"from_name"`
	// Hostname to use. Must be a valid and confirmed hostname. If omitted, default user hostname will be used.
	Host string `json:"host"`
	ID   int    `json:"id,string"`
	// Datetime the mailing was last paused at. NULL if mailing is not paused.
	Paused string `json:"paused"`
	// Number of recipients the mailing will be sent to. This can change as soon as the prepare process is completed.
	Recipients int `json:"recipients,string"`
	// Datetime the mailing was sent at. NULL if mailing is not yet sent.
	Sent string `json:"sent"`
	// Datetime to send the mailing at. If omitted, current datetime will be used.
	SendAt string  `json:"send_at"`
	Size   float64 `json:"size,string"`
	// Mailing status. Can be draft to mark if the mailing should be created as a draft for manual sending lateron or scheduled to schedule mailing rightaway.
	Status string `json:"status"`
	// Mailing subject. Will be NULL for A/B split mailings
	Subject string `json:"subject"`
	// Mailing subject. Will be NULL for non-A/B split mailings
	Title string `json:"title"`
	// Tracking domain used
	TrackingDomain string `json:"tracking_domain"`
	// Configured tracking parameters
	TrackingParam string `json:"tracking_param"`
	// Last modified datetime
	Updated string `json:"updated"`
}

// MailingDestination ...
type MailingDestination struct {
	// Specifies if the destination should be used as include or as exclude list. Please note that at least one include list needs to be specified. This feature may not be enabled for your account.
	Action string `json:"action"`
	// ID of recipientlist to send mailing to
	ID uint `json:"id"`
	// Currently, only “recipientlist” is supported
	Type string `json:"type"`
}

// MailingRecipient ...
// https://developer.rapidmail.wiki/documentation.html?urls.primaryName=Mailings#/MailingRecipients/get_mailings__mailing_id__stats_activity
type MailingRecipient struct {
	// Datetime the recipient marked the mail as abused or null if not marked
	Abused string `json:"abused"`
	// Birthday
	Birthdate string `json:"birthdate"`
	// Datetime the recipient was bounced or null if not bounced
	Bounced string `json:"bounced"`
	// Datetime the recipient first clicked a link in the mailing or null if not clicked
	Clicked string `json:"clicked"`
	// Email address
	Email string `json:"email"`
	// Extra 1
	Extra1 string `json:"extra1"`
	// Extra 10
	Extra10 string `json:"extra10"`
	// Extra 2
	Extra2 string `json:"extra2"`
	// Extra 3
	Extra3 string `json:"extra3"`
	// Extra 4
	Extra4 string `json:"extra4"`
	// Extra 5
	Extra5 string `json:"extra5"`
	// Extra 6
	Extra6 string `json:"extra6"`
	// Extra 7
	Extra7 string `json:"extra7"`
	// Extra 8
	Extra8 string `json:"extra8"`
	// Extra 9
	Extra9 string `json:"extra9"`
	// Firstname
	Firstname string `json:"firstname"`
	// Foreign/external ID of record
	ForeignID string `json:"foreign_id"`
	// Gender
	Gender string `json:"gender"`
	// ID
	ID int `json:"id,string"`
	// Lastname
	Lastname string `json:"lastname"`
	// Datetime the recipient received the mailing
	MailingReceived string `json:"mailing_received"`
	// Datetime the recipient first opened the mailing or null if not opened
	Opened      string `json:"opened"`
	RecipientID int    `json:"recipient_id,string"`
	// Datetime the recipient unsubscribed or null if not unsubscribed
	Unsubscribed string `json:"unsubscribed"`
	// Zipcode
	ZIP string `json:"zip"`
}

// MailingRecipientsResponse data
type MailingRecipientsResponse struct {
	APIResponse
	Embedded map[string][]MailingRecipient `json:"_embedded"`
}

// Recipients returns a slice of MailingRecipient embeded in response
func (mrr *MailingRecipientsResponse) Recipients() []MailingRecipient {
	return mrr.Embedded["mailingrecipients"]
}

// Search returns an instance of Mailingsreponse matching params and nil or nil and an error.
// Implements MailingsService interface
func (ms *mailingsService) Search(params MailingsRequestParams) (*MailingsResponse, error) {
	return ms.searchFunc(params)
}

func mailingsServiceSearchFunc(client Client) MailingsServiceSearchFunc {
	return func(params MailingsRequestParams) (*MailingsResponse, error) {
		data, err := client.Get(MailingsPath, params)
		if err != nil {
			return nil, err
		}

		result := &MailingsResponse{}
		return result, json.Unmarshal(data, &result)
	}
}

// SearchAll mailings matching params and return a slice of Mailing and nil
// or nil and an error.
func (ms *mailingsService) SearchAll(params MailingsRequestParams) ([]Mailing, error) {
	params.APIRequestParams.Page = 1

	result := []Mailing{}

	done := false
	for !done {
		response, err := ms.Search(params)
		if err != nil {
			return nil, err
		}
		result = append(result, response.Mailings()...)

		done = response.Page >= response.PageCount
		params.Page++
	}

	return result, nil
}

// FetchByID returns an instance if Mailing matching id and nil or nil and an error.
// Implements the MailingsService interface
func (ms *mailingsService) FetchByID(id int) (*Mailing, error) {
	return ms.fetchByIDFunc(id)
}

func mailingsServiceFetchByID(client Client) MailingsServiceFetchByIDFunc {
	return func(id int) (*Mailing, error) {
		path := fmt.Sprintf("%s/%d", MailingsPath, id)
		data, err := client.Get(path, nil)
		if err != nil {
			return nil, err
		}

		result := &Mailing{}
		return result, json.Unmarshal(data, &result)
	}
}

// MailingRecipients returns all recipients of mailing `mailingID` and nil or nil and an error
func (ms *mailingsService) Recipients(id int) (*MailingRecipientsResponse, error) {
	return ms.recipientsFunc(id)
}

func mailingsServiceRecipientsFunc(client Client) MailingsServiceRecipientsFunc {
	return func(id int) (*MailingRecipientsResponse, error) {
		path := fmt.Sprintf("%s/%d/stats/activity", MailingsPath, id)
		data, err := client.Get(path, nil)

		if err != nil {
			return nil, err
		}

		result := &MailingRecipientsResponse{}
		return result, json.Unmarshal(data, result)
	}
}

// SearchMailings uses global client to search for mailings matching params
// and returns a pointer of MailingsResponse and nil or nil and an error. Does not handle pagination.
func SearchMailings(params MailingsRequestParams) (*MailingsResponse, error) {
	return Mailings.Search(params)
}

// SearchAllMailings uses global client to search for all mailings matching params
// and returns a pointer of MailingsResponse and nil or nil and an error. Handles pagination.
func SearchAllMailings(params MailingsRequestParams) ([]Mailing, error) {
	return Mailings.SearchAll(params)
}

// FetchMailingByID used global client to fetch the mailing matching id
// and returns a pointer of Mailing and nil or nil and an error.
func FetchMailingByID(id int) (*Mailing, error) {
	return Mailings.FetchByID(id)
}

// Recipients uses global client to get recipients of mailing matching id
// and returns a pointer of MailingRecipientsResponse and nil or nil and an error
func Recipients(id int) (*MailingRecipientsResponse, error) {
	return Mailings.Recipients(id)
}

package rapidmail

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStringSliceToIntSlice(t *testing.T) {
	tt := []struct {
		Input  []string
		Output []int
		Error  error
	}{
		{
			Input:  []string{},
			Output: []int{},
		},
		{
			Input:  []string{"1"},
			Output: []int{1},
		},
	}

	for i, test := range tt {
		result, err := StringSliceToIntSlice(test.Input)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("\ntest: %d\nunexpected error: %v", i, err)
			}
			if !assert.Equal(t, test.Error.Error(), err.Error()) {
				t.Fatalf("\ntest: %d\nwant: %v\ngot: %v", i, test.Error.Error(), err.Error())
			}
		}

		if !assert.Equal(t, test.Output, result) {
			t.Fatalf("\ntest: %d\nwant: %v\ngot: %v", i, test.Output, result)
		}
	}
}

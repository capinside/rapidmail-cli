package rapidmail

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBlacklistsServiceSearch(t *testing.T) {
	tt := []struct {
		Params     BlacklistsRequestParams
		Body       []byte
		StatusCode int
		Result     *BlacklistsResponse
		Error      error
	}{
		{
			Params:     BlacklistsRequestParams{},
			StatusCode: http.StatusNotFound,
			Error:      fmt.Errorf(http.StatusText(http.StatusNotFound)),
		},
		{
			Params:     BlacklistsRequestParams{},
			Body:       []byte(`{"_embedded":{}}`),
			StatusCode: http.StatusOK,
			Result: &BlacklistsResponse{
				Embedded: map[string][]Blacklist{},
			},
			Error: nil,
		},
		{
			Params:     BlacklistsRequestParams{},
			Body:       []byte(`{"_links":{"self":{"href":"string"},"first":{"href":"string"},"last":{"href":"string"},"next":{"href":"string"},"prev":{"href":"string"}},"_embedded":{"blacklist":[{"id":"1","type":"email","pattern":"john.doe@example.net","source":"source_unknown","created":"2017-01-01 01:00:00","updated":"2017-01-01 01:00:00"}]},"page_count":1,"page_size":25,"total_items":560,"page":1}`),
			StatusCode: http.StatusOK,
			Result: &BlacklistsResponse{
				APIResponse: APIResponse{
					Links: map[string]map[string]string{
						"first": {"href": "string"},
						"last":  {"href": "string"},
						"next":  {"href": "string"},
						"prev":  {"href": "string"},
						"self":  {"href": "string"},
					},
					Page:       1,
					PageCount:  1,
					PageSize:   25,
					TotalItems: 560,
				},
				Embedded: map[string][]Blacklist{
					"blacklist": {
						{
							ID:      1,
							Type:    "email",
							Pattern: "john.doe@example.net",
							Source:  "source_unknown",
							Created: "2017-01-01 01:00:00",
							Updated: "2017-01-01 01:00:00",
						},
					},
				},
			},
			Error: nil,
		},
	}

	for i, test := range tt {
		server := setup(BlacklistPath, test.StatusCode, test.Body)

		result, err := SearchBlacklists(test.Params)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("\ntest: %d\nunexpected error: %v", i, err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Result, result)
		}

		server.Close()
	}
}

func TestBlacklistsServiceFetchByID(t *testing.T) {
	tt := []struct {
		ID         int
		Body       []byte
		StatusCode int
		Result     *Blacklist
		Error      error
	}{
		{
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			ID:         1,
			Body:       []byte(`{"id":"1","type":"email","pattern":"john.doe@example.net","source":"source_unknown","created":"2017-01-01 01:00:00","updated":"2017-01-01 01:00:00"}`),
			StatusCode: http.StatusOK,
			Result: &Blacklist{
				ID:      1,
				Type:    "email",
				Pattern: "john.doe@example.net",
				Source:  "source_unknown",
				Created: "2017-01-01 01:00:00",
				Updated: "2017-01-01 01:00:00",
			},
			Error: nil,
		},
	}

	for i, test := range tt {
		server := setup(fmt.Sprintf("%s/%d", BlacklistPath, test.ID), test.StatusCode, test.Body)

		result, err := FetchBlacklistByID(test.ID)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("\ntest: %d\nunexpected error: %v", i, err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Error, err)
				}
			}
		}

		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Result, result)
		}

		server.Close()
	}
}

func TestBlacklistServiceSearchAll(t *testing.T) {
	tt := []struct {
		Params     BlacklistsRequestParams
		StatusCode int
		Body       []byte
		Result     []Blacklist
		Error      error
	}{
		{
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			Body:       []byte(`{"_embedded":{"blacklist":[]}}`),
			StatusCode: http.StatusOK,
			Result:     []Blacklist{},
		},
	}

	for i, test := range tt {
		server := setup(BlacklistPath, test.StatusCode, test.Body)

		result, err := SearchAllBlacklists(test.Params)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("\ntest: %d\nunexpected error: %v", i, err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Error, err)
				}
			}
		}

		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\ntest: %d\nwant: %v\ngot:  %v", i, test.Result, result)
		}

		server.Close()
	}

}

package cmd

import (
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/capinside/rapidmail-cli/cmd/blacklists"
	"gitlab.com/capinside/rapidmail-cli/cmd/mailings"
	"gitlab.com/capinside/rapidmail-cli/pkg/rapidmail"
)

var (
	rootCmd = &cobra.Command{
		Use:               "rapidmail",
		Short:             "RapidMail command line utility",
		PersistentPreRunE: persistentPreRunEFunc,
	}

	username string
	password string
	url      string
	logLevel string
)

func init() {
	rootCmd.PersistentFlags().StringVarP(&username, "username", "", "", "Username to Authenticate on Rapidmail API")
	rootCmd.PersistentFlags().StringVarP(&password, "password", "", "", "Password to Authenticate on Rapidmail API")
	rootCmd.PersistentFlags().StringVarP(&url, "url", "", rapidmail.BaseURLV3, "URL on Rapidmail API")
	rootCmd.PersistentFlags().StringVarP(&logLevel, "log-level", "", "info", "Log level. Must be one of: panic,fatal,error,warn,info,debug,trace")

	viper.SetEnvPrefix("RAPIDMAIL_API")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	viper.BindPFlag("log-level", rootCmd.PersistentFlags().Lookup("log-level"))
	viper.BindPFlag("url", rootCmd.PersistentFlags().Lookup("url"))
	viper.BindPFlag("username", rootCmd.PersistentFlags().Lookup("username"))
	viper.BindPFlag("password", rootCmd.PersistentFlags().Lookup("password"))

	rootCmd.AddCommand(
		blacklists.Command(),
		mailings.Command(),
	)
}

func Execute() error {
	return rootCmd.Execute()
}

func persistentPreRunEFunc(cmd *cobra.Command, args []string) error {
	return rapidmail.Init(viper.GetString("url"), viper.GetString("username"), viper.GetString("password"), viper.GetString("log-level"))
}

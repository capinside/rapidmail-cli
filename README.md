# Rapidmail Golang client

Rapidmail API client to work with [Rapidmail API](https://rapidmail.de).

## Installation

```shell
$ go install gitlab.com/capinside/rapidmail-cli@latest
```

## Usage

Following environment variables are used by the client:

| Name                   | Description                   | Default                      | Required |
| :--------------------- | :---------------------------- | :--------------------------- | :------: |
| RAPIDMAIL_API_URL      | URL of Rapidmail API          | `https://apiv3.emailsys.net` | `false`  |
| RAPIDMAIL_API_USERNAME | Username used to authenticate | `""`                         |  `true`  |
| RAPIDMAIL_API_PASSWORD | Password used to authenticate | `""`                         |  `true`  |


### Command line utility

```shell
$ rapidmail-cli -h

RapidMail command line utility

Usage:
  rapidmail [command]

Available Commands:
  blacklists  Blacklist commands
  completion  Generate the autocompletion script for the specified shell
  help        Help about any command
  mailings    Mailing commands

Flags:
  -h, --help               help for rapidmail
      --log-level string   Log level. Must be one of: panic,fatal,error,warn,info,debug,trace (default "info")
      --password string    Password to Authenticate on Rapidmail API
      --url string         URL on Rapidmail API (default "https://apiv3.emailsys.net")
      --username string    Username to Authenticate on Rapidmail API

Use "rapidmail [command] --help" for more information about a command.
```

### Use the default initialized Client

```go
package main

import (
	"fmt"

	"gitlab.com/capinside/rapidmail-cli/pkg/rapidmail"
)

func main() {
	blacklists,err := rapidmail.Blacklists.SearchAll(rapidmail.BlacklistsSearchParams{})
	if err != nil {
		panic(err)
	}

	fmt.Println(leads)
}
```

### Initialize a new Client with service

```go
package main

import (
	"fmt"

	"gitlab.com/capinside/rapidmail-cli/pkg/rapidmail"
)

func main() {
	// init config
	cfg, err := rapidmail.NewConfig("https://apiv3.emailsys.net", "<username>", "<password>")
	if err != nil {
		panic(err)
	}

	// init client
	client := rapidmail.NewClient(*cfg)

	// init blacklists service
	bs := rapidmail.NewBlacklistsService(client)

	// search for all blacklists
	blacklists, err := bs.SearchAll(rapidmail.BlacklistsRequestParams{})
	if err != nil {
		panic(err)
	}

	fmt.Println(blacklists)
}
```

## Note 

Only a limited set of all API functions are currently implemented, however the existing framework makes implementing further functions extremely lightweight and easy.

## Contributing

Bug reports and pull requests are welcome. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](./CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).


## Author

[CAPinside GmbH](https://capinside.com)
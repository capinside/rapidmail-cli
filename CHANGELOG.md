## [1.0.1](https://gitlab.com/capinside/rapidmail-cli/compare/1.0.0...1.0.1) (2022-11-24)


### Bug Fixes

* Docu ([11ae4f2](https://gitlab.com/capinside/rapidmail-cli/commit/11ae4f2e636c565c0552f2b36055c8765ef23331))

# 1.0.0 (2022-11-24)


### Bug Fixes

* Makefile ([18a3426](https://gitlab.com/capinside/rapidmail-cli/commit/18a342613fa7108e10ed6f8112c2c0a852b417dc))
* username, password and url can't be passed to CLI ([2e388a7](https://gitlab.com/capinside/rapidmail-cli/commit/2e388a728b79ea0e6bd53963c7bd475810a30bbb))


### Features

* Integrate Rapidmail client API ([05ff43a](https://gitlab.com/capinside/rapidmail-cli/commit/05ff43ab5b7ba87970bf67d5fecb827d21282493))
